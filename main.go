// Utility to sort and extract the best blast match

package main

import (
    "flag"
    "fmt"
    "log"
    "strings"
    "sync"
    "time"

    "bitbucket.org/GottySG36/blast"
    "bitbucket.org/GottySG36/utils"
)

var inp  = flag.String("i", "", "Input file paths, comma seperated")
var outp = flag.String("o", "", "Output file in which to extract the best its")
var filtKing = flag.String("k", "NONE!", "Terms to ignore in row 'Kingdom', comma seperated")
var filtNames = flag.String("s", "NONE!", "Terms to ignore in row 'Scinames', comma seperated")

func main() {
    flag.Parse()

    if *inp == "" {
        log.Fatalf("Error -:- Input : No files specified. At least one file must be specified with option '-i'\n")
    }
    files := strings.Split(*inp, ",")
    out := []string{}
    if *outp != "" {
        out = strings.Split(*outp, ",")
    }
    if len(out) != len(files) {
        log.Fatalf("Error -:- Output : Number of output directories does not match the number of input files\n")
    }

    var lock sync.Mutex
    blastRes := make(map[string]blast.BlastC)
    sortedBlast := make(map[string]blast.BlastC, len(files))
    sortedBlastDup := make(map[string]blast.Duplicated, len(files))

    quit := make(chan struct{})
    go utils.Spinner("Loading", 100 * time.Millisecond, quit)
    for _, file := range files {
        b, err := blast.LoadBlastC(file)
        if err != nil {
            log.Fatalf("Error -:- LoadBlastC : %v\n", err)
        }

        lock.Lock()
        blastRes[file] = b
        lock.Unlock()

    }
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf("\r\nDone!\n\n")

    quit = make(chan struct{})
    go utils.Spinner("Sorting", 100 * time.Millisecond, quit)
    for file, bl := range blastRes {
        blast.OrderedByC(blast.QcovsC, blast.PidentC, blast.IncEvalueC).Sort(bl)
        e, dup, err := blast.ExtractBestC(bl, *filtKing, *filtNames)
        if err != nil {
            log.Fatalf("Error -:- ExtractBestC : %v\n", err)
        }
        lock.Lock()
        sortedBlast[file] = e
        sortedBlastDup[file] = dup
        lock.Unlock()
    }
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf("\r\nDone!\n\n")

    // Writing blasts to file
    for i, file := range files {
        quit = make(chan struct{})
        go utils.Spinner("Writing output", 100 * time.Millisecond, quit)
        err := sortedBlast[file].WriteC(out[i])
        if err != nil {
            log.Fatalf("Error -:- Write output : %v\n", err)
        }
        err = sortedBlastDup[file].WriteDupC(out[i])
        if err != nil {
            log.Fatalf("Error -:- Write duplicated output : %v\n", err)
        }
        close(quit)
        time.Sleep(250 * time.Millisecond)
        fmt.Printf("\r\nDone!\n\n")
        fmt.Printf("Output for file %v : %v\n", file, out[i])
    }
}
