module bitbucket.org/GottySG36/blast_sorter

go 1.13

require (
	bitbucket.org/GottySG36/blast v0.1.1
	bitbucket.org/GottySG36/pangenome v0.1.1 // indirect
	bitbucket.org/GottySG36/utils v0.1.0
)
